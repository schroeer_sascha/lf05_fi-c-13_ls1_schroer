package eingabeAusgabe;

import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ScannerTests {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie Ihren Namen ein. :)");
		String name = myScanner.next();
		
		System.out.println("Bitte geben Sie Ihr Alter ein. :)");
		byte alter = myScanner.nextByte();
		
		System.out.println("Dankesch�n. Du hei�t "+name+" und bist "+alter+" Jahre alt.");
		
		myScanner.close();
		
		
		// SPIELEREIEN
		AtomicInteger i = new AtomicInteger(alter);
		
		ScheduledExecutorService counter = Executors.newScheduledThreadPool(1);
		
		counter.scheduleAtFixedRate(() -> {
			i.set(i.get()-1);
			
			if(i.get() == 0) {
				System.out.println("HAHAHAHAH STOP!");
				System.exit(0);
			}
			
			System.out.println("Guck mal: "+i.get()+" Jahre!");
			
		}, 1, 1, TimeUnit.SECONDS);
	}

}
