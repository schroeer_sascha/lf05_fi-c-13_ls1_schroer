package eingabeAusgabe;

import java.util.Scanner;

public class A2_Ubung {

	public static void main(String[] args) {
		// Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    int zahl1 = myScanner.nextInt();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    int zahl2 = myScanner.nextInt();  
	     
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    int ergebnis = zahl1 + zahl2;
	    int ergebnisMinus = zahl1 - zahl2;
	    long ergebnisMult = zahl1 * zahl2;
	    double ergebnisDiv= (double) zahl1 / (double) zahl2;
	     
	    System.out.println("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);   
	    System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnisMinus);
	    System.out.println(zahl1 + " x " + zahl2 + " = " + ergebnisMult);
	    System.out.println(zahl1 + " : " + zahl2 + " = " + ergebnisDiv);
	 
	    myScanner.close();
	}
	
}
