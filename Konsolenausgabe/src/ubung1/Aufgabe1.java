package ubung1;

public class Aufgabe1 {
  
	public static void main(String[] args) {
		System.out.print("Das ist ein \"Test\".\n");
		System.out.print("Mir geht es gut.");
		
/*		Unterschied zwischen print() und println() ist im groben darin, dass print() in einer Linie ausgibt.
		Er nutzt dafür keine Zeilenumbrüche. Bei println() werden Zeilenumbrüche mit abgeschickt. */
	}
	
}
