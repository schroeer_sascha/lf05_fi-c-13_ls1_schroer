package ubung2;

public class Aufgabe2 {

	public static void main(String[] args) {
		{
			System.out.printf("%-5s", "0!");
			System.out.print("=");
			System.out.printf("%-19s", "");
			System.out.print("=");
			System.out.printf("%4s\n", 1, -1);
		}
		
		{
			System.out.printf("%-5s", "1!");
			System.out.print("=");
			System.out.printf("%-19s", " 1");
			System.out.print("=");
			System.out.printf("%4s\n", 1, -1);
		}
		
		{
			System.out.printf("%-5s", "2!");
			System.out.print("=");
			System.out.printf("%-19s", " 1 * 2");
			System.out.print("=");
			System.out.printf("%4s\n", 2, -1);
		}
		
		{
			System.out.printf("%-5s", "3!");
			System.out.print("=");
			System.out.printf("%-19s", " 1 * 2 * 3");
			System.out.print("=");
			System.out.printf("%4s\n", 6, -1);
		}
		
		{
			System.out.printf("%-5s", "4!");
			System.out.print("=");
			System.out.printf("%-19s", " 1 * 2 * 3 * 4");
			System.out.print("=");
			System.out.printf("%4s\n", 24, -1);
		}
		
		{
			System.out.printf("%-5s", "5!");
			System.out.print("=");
			System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5");
			System.out.print("=");
			System.out.printf("%4s\n", 120, -1);
		}
	}
	
}
